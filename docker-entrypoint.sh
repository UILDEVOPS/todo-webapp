#!/bin/bash
set -e

# Remove a potentially pre-existing server.pid for Rails.
rm -f /myapp/tmp/pids/server.pid

# Check if the required environment variables are set
if [ -z "$DATABASE_URL" ]; then
  echo "Error: DATABASE_URL is not set."
  exit 1
fi

# Check if bundler is installed
if ! gem list -i bundler > /dev/null; then
  echo "Bundler is not installed. Installing..."
  gem install bundler
fi

# Check if Rails is installed
if ! bundle list | grep "rails " > /dev/null; then
  echo "Rails is not installed. Installing..."
  bundle config set without 'production'
  bundle install
fi

# Check if bundle exec was successful
if ! bundle exec rails -v > /dev/null; then
  echo "Error: bundle exec rails command failed. Exiting..."
  exit 1
fi

# Create the database if it doesn't exist
if bundle exec rails db:exists; then
  echo "Database already exists."
else
  echo "Creating database..."
  bundle exec rails db:create
fi

# Run migrations
echo "Running migrations..."
bundle exec rails db:migrate

# Load seed data if necessary
if bundle exec rails db:seed; then
  echo "Seed data loaded successfully."
fi

# Execute the given command
exec "$@"
